const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = (env, argv) => {
    const isProduction = argv.mode === 'production';
    return {
        mode: isProduction ? 'production' : 'development',
        entry: './src/main.js',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'bundle.js',
        },
        module: {
            rules: [
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader'],
                },
            ],
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './index.html',
            }),
            new CopyWebpackPlugin({
                patterns: [
                    {
                        from: 'src/assets',
                        to: 'assets',
                    },
                ],
            }),
            ...(isProduction
                ? [
                    new TerserPlugin({

                    }),
                ]
                : []),
        ],
        devServer: {
            static: path.join(__dirname, 'dist'),
            hot: true,
            proxy: {
                "/subscribe": {
                  target: "http://localhost:3000",
                  changeOrigin: true,
                },
                "/community": {
                  target: "http://localhost:3000",
                  changeOrigin: true,
                },
                "/unsubscribe": {
                  target: "http://localhost:3000",
                  changeOrigin: true,
                },
              },
        },
        stats: {
            children: true,
        },
    };
};
