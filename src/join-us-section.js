import validate from './email-validator.js';

// Factory class for creating sunscribtion section
const SectionCreator = {
    create(type) {
        if (type === 'standard') {
            return new StandardProgram();
        } if (type === 'advanced') {
            return new AdvancedProgram();
        }
        throw new Error('Invalid program type.');
    },
};

// Standard program class

function StandardProgram() {
    this.section = document.createElement('div');
    this.section.id = 'joinProgram';
    this.section.classList.add('standard-program');

    const heading = document.createElement('h2');
    heading.textContent = 'Join Our Program';

    const description = document.createElement('p');
    description.textContent = 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua';

    const form = document.createElement('form');

    const emailInput = document.createElement('input');
    emailInput.type = 'email';
    emailInput.name = 'email';
    emailInput.placeholder = 'Email';
    emailInput.classList.add('email-input');

    const subscribeButton = document.createElement('button');
    subscribeButton.type = 'submit';
    subscribeButton.textContent = 'Subscribe';
    subscribeButton.classList.add('subscribe-button');

    form.appendChild(emailInput);
    form.appendChild(subscribeButton);

    this.section.appendChild(heading);
    this.section.appendChild(description);
    this.section.appendChild(form);
}

StandardProgram.prototype.remove = function () {
    if (this.section.parentNode) {
        this.section.parentNode.removeChild(this.section);
    }
};

// Advanced program class

function AdvancedProgram() {
    this.section = document.createElement('div');
    this.section.id = 'joinProgram';
    this.section.classList.add('advanced-program');

    const heading = document.createElement('h2');
    heading.textContent = 'Join Our Advanced Program';

    const description = document.createElement('p');
    description.textContent = 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';

    const form = document.createElement('form');
    form.addEventListener('submit', this.handleFormSubmit.bind(this));

    const emailInput = document.createElement('input');
    emailInput.type = 'email';
    emailInput.name = 'email';
    emailInput.placeholder = 'Email';
    emailInput.classList.add('email-input');

    const subscribeButton = document.createElement('button');
    subscribeButton.type = 'submit';
    subscribeButton.textContent = 'Subscribe to Advanced Program';
    subscribeButton.classList.add('subscribe-button');
    subscribeButton.classList.add('subscribe-button-advanced');

    form.appendChild(emailInput);
    form.appendChild(subscribeButton);

    this.section.appendChild(heading);
    this.section.appendChild(description);
    this.section.appendChild(form);
}

AdvancedProgram.prototype.remove = function () {
    if (this.section.parentNode) {
        this.section.parentNode.removeChild(this.section);
    }
};

// Add the section to DOM

export default function addJoinOurProgramSection(type) {
    const existingContent = document.getElementById('contentToAdd');

    const programSection = SectionCreator.create(type);

    let isSubscribed = localStorage.getItem('isSubscribed') === 'true';

    // hide email input and change subscribe btn
    function updateUI() {
        const emailInput = programSection.section.querySelector('.email-input');
        const subscribeButton = programSection.section.querySelector('.subscribe-button');

        if (isSubscribed) {
            emailInput.style.display = 'none';
            subscribeButton.textContent = 'Unsubscribe';
        } else {
            emailInput.style.display = 'inline-block';
            subscribeButton.textContent = 'Subscribe';
        }
    }

    updateUI();

    const emailInput = programSection.section.querySelector('.email-input');
    const subscribeButton = programSection.section.querySelector('.subscribe-button');

    existingContent.insertAdjacentElement('afterend', programSection.section);

    // handle form submit

    const form = document.querySelector('form');

    form.addEventListener('submit', (event) => {
        event.preventDefault();
        const formData = new FormData(event.target);
        const emailValue = formData.get('email');

        const isValidEmail = validate(emailValue);

        if (subscribeButton.textContent === 'Subscribe') {
            if (isValidEmail) {
                localStorage.setItem('subscriptionEmail', emailValue);

                localStorage.setItem('isSubscribed', 'true');

                isSubscribed = true;

                updateUI();

                sendSubscribeRequest(emailValue);
            }
        } else {
            localStorage.removeItem('subscriptionEmail');

            localStorage.setItem('isSubscribed', 'false');

            isSubscribed = false;

            emailInput.value = '';

            updateUI();
            console.log('unsubscribe');

            sendUnsubscribeRequest();
        }
    });

    // Function to handle subscribe request
    async function sendSubscribeRequest(email) {
        try {
            const subscribeButton = programSection.section.querySelector('.subscribe-button');

            // Disable the button and change its style
            subscribeButton.disabled = true;
            subscribeButton.style.opacity = '0.5';

            const response = await fetch('http://localhost:8080/subscribe', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ email }),
            });

            if (!response.ok) {
                if (response.status === 422) {
                    const errorPayload = await response.json();
                        window.alert(`Subscription Error: ${errorPayload.error}`);
                } else {
                    console.error('Failed to send subscribe request:', response.statusText);
                }
            }

            // After the request is complete
            subscribeButton.disabled = false;
            subscribeButton.style.opacity = '1';
        } catch (error) {
            console.error('An error occurred:', error);
        }
    }

    // Function to handle unsubscribe request
    async function sendUnsubscribeRequest() {
        try {
            const unsubscribeButton = programSection.section.querySelector('.subscribe-button');

            // Disable the button and change its style
            unsubscribeButton.disabled = true;
            unsubscribeButton.style.opacity = '0.5';

            const response = await fetch('http://localhost:8080/unsubscribe', {
                method: 'POST',
            });

            if (!response.ok) {
                console.error('Failed to send unsubscribe request:', response.statusText);
            }

            // After the request is complete
            unsubscribeButton.disabled = false;
            unsubscribeButton.style.opacity = '1';
        } catch (error) {
            console.error('An error occurred:', error);
        }
    }
}
